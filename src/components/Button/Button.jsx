import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import './Button.scss';

function Button(props) {
    const { className, type, click, children, ...restProps } = props;

    return (
        <button className={cn('button', className)} onClick={click} type={type} {...restProps}>
            {children}
        </button>
    );
}

Button.defaultProps = {
    type: 'button',
};

Button.propTypes = {
    className: PropTypes.string,
    click: PropTypes.func,
    children: PropTypes.any,
};

export default Button;