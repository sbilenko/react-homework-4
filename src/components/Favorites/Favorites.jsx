import React from 'react';
import PropTypes from 'prop-types';

import './Favorites.scss';

import { ReactComponent as Favorite } from '../Header/icons/favorite.svg';

function Favorites({ listItems, favorites, setFavorites }) {
    const deleteFavorite = article => {
        if (favorites.includes(article)) {
            const updateFavorites = favorites.filter(el => el !== article);
            setFavorites([...updateFavorites]);
        }
    };

    return (
        <div className="favorites__content">
            {favorites.length > 0 && <div className="favorites__title">FAVORITES</div>}
            {favorites.length === 0 && <div className="favorites__title favorites__empty">FAVORITES is Empty</div>}
            <div className="favorites__items">
                {listItems.map(
                    (item, index) =>
                        favorites.includes(item) && (
                            <div className="favorites__item" key={index}>
                                <span className="favorites__item-article">{item.article}</span>
                                <h1 className="favorites__item-title">{item.title}</h1>
                                <img className="favorites__item_img" src={item.url} alt="item-img" />
                                <div className="favorites__item-color">{item.color}</div>
                                <div className="favorites__item-price">{item.price} ₴</div>
                                <div className="favorites__item-icon--wrapper">
                                    <Favorite
                                        className="favorites__item-icon"
                                        onClick={() => deleteFavorite(item)}
                                    />
                                </div>
                            </div>
                        )
                )}
            </div>
        </div>
    );
}

Favorites.propTypes = {
    listItems: PropTypes.array,
    favorites: PropTypes.array,
    setFavorites: PropTypes.func,
};

export default Favorites;
