import React from 'react';
import PropTypes from 'prop-types';
import { Routes, Route } from 'react-router-dom';

import { ListItems } from './ListItems';
import { Favorites } from '../Favorites';
import { Cart } from '../Cart';
import { NotPage } from '../NotPage';

import './Main.scss';

function Main({ listItems, cart, setCart }) {

    // const addToFavorites = item => {
    //     if (!favorites.includes(item)) {
    //         setFavorites([...favorites, item]);
    //     } else {
    //         const updateFavorites = favorites.filter((el) => el !== item);
    //         setFavorites([...updateFavorites]);
    //     }
    // };

    const addToCart = article => {
        if (!cart.includes(article)) {
            console.log(article);
            setCart([...cart, article]);
        }
    };

    return (
        <div className="main__wrapper">
            <div className="container">
                <div className="main">
                    <Routes>
                        <Route
                            path="/"
                            element={
                                <ListItems
                                    listItems={listItems}
                                    // favorites={favorites}
                                    cart={cart}
                                    // addToFavorites={addToFavorites}
                                    addToCart={addToCart}
                                />
                            }
                        />
                        <Route
                            path="/favorites"
                            element={
                                <Favorites listItems={listItems} />
                            }
                        />
                        <Route path="/cart" element={<Cart listItems={listItems} cart={cart} setCart={setCart} />} />
                        <Route path="*" element={<NotPage />} />
                    </Routes>
                </div>
            </div>
        </div>
    );
}

Main.propTypes = {
    listItems: PropTypes.array,
    favorites: PropTypes.array,
    setFavorites: PropTypes.func,
    cart: PropTypes.array,
    setCart: PropTypes.func,
};

export default Main;
