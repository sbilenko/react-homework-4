import { configureStore } from '@reduxjs/toolkit';

import listItemsSlice from './slices/listItems.slice';
import favoritesSlice from './slices/favorites.slice';
import cartSlice from './slices/cart.slice';

export const store = configureStore({
    reducer: {
        listItems: listItemsSlice,
        favorites: favoritesSlice, 
        cart: cartSlice, 
    },
});
