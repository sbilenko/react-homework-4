import React from 'react';
import { Routes, Route } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import { actionFetchListItems } from './redux/slices/listItems.slice';
// import { actionSetListItems } from './redux/slices/listItemsSlice';
import { actionSetFavorites } from './redux/slices/favorites.slice';
import { actionSetCart } from './redux/slices/cart.slice';

import { Header } from './components/Header';
import { Main } from './components/Main';
import { Footer } from './components/Footer';
// import { sendRequest } from './helpers/sendRequest';

import './App.css';

function App() {
    // console.log('Спрацював Global');
    const dispatch = useDispatch();

    const listItems = useSelector(state => state.listItems.listItems);
    // console.log('LISTITEMS from Redux', listItems);
    const favorites = useSelector(state => state.favorites.favorites);
    // console.log('FAVORITES from Redux', favorites);
    const cart = useSelector(state => state.cart.cart);
    // console.log('CART from Redux', cart);

    const [qtyFavorites, setQtyFavorites] = React.useState(favorites.length);
    const [qtyCart, setQtyCart] = React.useState(cart.length);

    // const setListItems = data => {
    //     dispatch(actionSetListItems(data));
    // };

    const setFavorites = data => {
        dispatch(actionSetFavorites(data));
    };

    const setCart = data => {
        dispatch(actionSetCart(data));
    };

    React.useEffect(() => {
        dispatch(actionFetchListItems);
        console.log('FETCH listItems', dispatch(actionFetchListItems));

        const favoritesLocalStorage = localStorage.getItem('favorites');
        const cartLocalStorage = localStorage.getItem('cart');

        if (favoritesLocalStorage) {
            setFavorites(JSON.parse(favoritesLocalStorage));
            // console.log('Беруться дані з favoritesLocalStorage');
        }

        if (cartLocalStorage) {
            setCart(JSON.parse(cartLocalStorage));
            // console.log('Беруться дані з cartLocalStorage');
        }
        // console.log('Спрацювала 1 загрузка');
    }, []);

    React.useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites));
        setQtyFavorites(favorites.length);
        // console.log('Спрацював useEffect of Favorites');
    }, [favorites]);

    React.useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
        setQtyCart(cart.length);
        // console.log('Спрацював useEffect of Cart');
    }, [cart]);

    return (
        <div className="app-wrapper">
            <Header qtyFavorites={qtyFavorites} qtyCart={qtyCart} />
            <Routes>
                <Route
                    path="*"
                    element={
                        <Main
                            listItems={listItems}
                            // favorites={favorites}
                            // setFavorites={setFavorites}
                            cart={cart}
                            setCart={setCart}
                        />
                    }
                />
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
